# STAR TREK - The Imprisioned Man 

 The well known "The Hanged Man" game, Star Trek LCARS inspired.

### About this project

 This is merely an academic exercise. Its purpose is to gain an understanding of how the Codeigniter framework works.
 Being a Star Trek fan, I decided to make "The Hanged Man" game in a 23rd Century styled GUI.
 
 The project is divided in two areas: backoffice, where an administrator can control several game-related aspects, as well as users, etc., and frontend, where the games actually take place.
 
### Disclaimer

 I am not in anyway associated or affiliated with whoever holds the Star Trek franchise, or any of its subsidiaries. 
 I am not making any kind of money or profit out of this project. 
 As stated above, this is merely an academic, fan-made work. 

 This software has no license and therefore cannot be considered free software. You are free to download it for educational purposes only.
 
### Note to whom it may concern
 
 This project will be migrated to Laravel, to a new repository nearby...
