/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
   $('#idate').focus(function () {
       $('#idate').datepicker('setDate', '-1');
   }); 
   
   $('#fdate').focus(function () {
       $('#fdate').datepicker('setDate', 'today');
   });
});

function setDelete(what, value) {
    $('.row.confirm').fadeIn('slow', function () {
        var href = $('.row.confirm a').attr('href');
        href += 'del_' + what + '/' + value;
        $('.row.confirm a').attr('href', href);
    });
}

function resetConfigXOR(element) {
    var elements = [];   
    
    elements['attempts'] = 'timer';
    elements['timer']    = 'attempts';
    
    if ($(element).val() > 0) {
        $('#' + elements[$(element).attr('id')]).val(0);
    }
}