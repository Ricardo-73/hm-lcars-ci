<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of User
 *
 * @author ricardo
 */
class UserModel extends CI_Model {
    
    public function login($user, $pass) {
        $pass = sha1($pass);
        
        $this->db->select('user_id, username, name, email, is_admin, created, updated, last_login');
        $this->db->from('users');
        $this->db->where('username', $user);
        $this->db->where('password', $pass);
        $this->db->where('is_active', 1);
        
        $qry = $this->db->get();
        $row = $qry->row_array();
        
        if ($row) {
            $login = array(
                'last_login'   => date('Y-m-d H:i:s'),
                'is_logged_in' => 1
            );
            
            $this->db->where('user_id', $row['user_id']);
            $this->db->update('users', $login);
        }
        
        return $row;
    }
    
    public function info($id) {
        $this->db->select('user_id, username, name, email, is_active, games_played, score, wins, lost, created, updated, last_login');
        $this->db->from('users u');
        $this->db->join('user_stats s', 's.id_user = u.user_id', 'INNER');
        $this->db->where('user_id', $id);
        
        $qry = $this->db->get();
        $info = $qry->first_row();
        
        $this->db->select('COUNT(game_id) AS pending', FALSE);
        $this->db->from('games');
        $this->db->where('is_active', 1);
        $this->db->where('(player_1 = '.$id.' OR player_2 = '.$id.')');
        
        $qry = $this->db->get();
        $qry = $qry->first_row();
        $info->pending = $qry->pending;
        
        return $info;
    }
    
    public function statistics($id) {
        $this->db->select('GREATEST(MAX(player1_score), MAX(player2_score)) AS highest', FALSE);
        $this->db->select('LEAST(MIN(player1_score), MIN(player2_score)) AS lowest', FALSE);
        
        $qryString = "((SELECT SUM(player1_score) FROM games WHERE player_1 = 2 AND is_active = 0)"; 
        $qryString .= " + ";
        $qryString .= "(SELECT SUM(player2_score) FROM games WHERE player_2 = 2 AND is_active = 0))";
        $qryString .= " / COUNT(game_id) AS average";
        
        $this->db->select($qryString, FALSE);
        
        $this->db->from('games');
        $this->db->join('users u1', 'u1.user_id = player_1', 'INNER');
        $this->db->join('users u2', 'u2.user_id = player_2', 'INNER');
        $this->db->where('u1.user_id', $id);
        $this->db->or_where('u2.user_id', $id);
        
        $qry = $this->db->get();        
        return $qry->first_row();        
    }
    
    public function user($id) {
        $this->db->select('user_id, username, name, email, is_active');
        $this->db->from('users');
        $this->db->where('user_id', $id);
        
        $qry = $this->db->get();
        $user = $qry->first_row();
        
        return $user;
    }
    
    public function create($register) {
        $register['password'] = sha1($register['password']);
        $ins = $this->db->insert('users', $register);
        
        if ($ins) {
            $user = array(
                'user_id'  => $this->db->insert_id(),
                'username' => $register['username'],
                'name'     => $register['name'],
                'email'    => $register['email'],
                'is_admin' => 0,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            );
            
            return $user;
        }
        else
            return FALSE;
    }
    
    public function delete($user) {
        $del = $this->db->delete('users', array('user_id' => $user));
        return $del;
    }
    
    public function update($user) {
        $user['password'] = sha1($user['password']);
        
        $this->db->set($user);
        $this->db->where('user_id', $user['user_id']);
        $upd = $this->db->update('users');
        
        return $upd;
    }
    
    public function logout($user) {
        $this->db->set($user);
        $this->db->where('user_id', $user['user_id']);
        $this->db->update('users');
    }

    public function get_users($limit, $start, $search = NULL) {
        $this->db->select('user_id, username, name, created, score, is_admin, is_active');
        $this->db->from('users');
        $this->db->join('user_stats', 'id_user = user_id', 'INNER');
        
        if ($search) {
            if (!empty($search->user_id))    $this->db->where('user_id', $search->user_id);
            if (!empty($search->username))   $this->db->like('username', $search->username);
            if (!empty($search->name))       $this->db->like('name', $search->name);
            if (!empty($search->created[0])) $this->db->where('created >= ', $search->created[0]);
            if (!empty($search->created[1])) $this->db->where('created <= ', $search->created[1]);
            if (!empty($search->is_active) || $search->is_active == 0) $this->db->where('is_active', $search->is_active);
        }
        
        $this->db->limit($limit, $start);
        
        $qry = $this->db->get();
        $users = $qry->result_object();
        
        return $users;
    }
    
    public function count_users() {
        return $this->db->count_all('users');
    }
}
