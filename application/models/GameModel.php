<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GameModel
 *
 * @author ricardo
 */
class GameModel extends CI_Model {
    
    public function game($id) {
        
    }
    
    public function info($id) {
        $this->db->select("g.game_id, CONCAT(c.config_id, ' @ ', c.created) AS config", FALSE);
        $this->db->select("u1.username AS player_1, IF (player_2 != 0, u2.username, 'N/A') AS player_2", FALSE);
        $this->db->select("player1_score, player2_score, started, IF (g.is_active != 0, finished, 'N/A') AS finished, last_saved, g.is_active", FALSE);
        $this->db->select('CASE WHEN player1_score > player2_score THEN player_1 
                                WHEN player2_score > player1_score THEN player_2 END AS lead_winner', FALSE);
        $this->db->select("IF (g.is_active = 0, DATEDIFF(finished, started), DATEDIFF(NOW(), started)) AS elapsed", FALSE);        
        $this->db->from('games g');
        $this->db->join('configs c', 'c.config_id = g.config', 'INNER');
        $this->db->join('users u1', 'u1.user_id = g.player_1', 'INNER');
        $this->db->join('users u2', 'u2.user_id = g.player_2', 'LEFT OUTER');
        $this->db->where('g.game_id', $id);
        
        $qry = $this->db->get();
        $game = $qry->first_row();
        
        return $game;
    } 
    
    public function delete($game) {
        $del = $this->db->delete('games', array('game_id' => $game));
        return $del;
    }
    
    public function get_games($limit, $start, $search = NULL) {
        $this->db->select("g.game_id, CONCAT(c.config_id, ' @ ', c.created) AS config", FALSE);
        $this->db->select("u1.username AS player_1, IF (player_2 != 0, u2.username, 'N/A') AS player_2", FALSE);
        $this->db->select('CASE WHEN player1_score > player2_score THEN player_1 
                                WHEN player2_score > player1_score THEN player_2 END AS lead_winner', FALSE);
        $this->db->select("started, IF (g.is_active != 0, finished, 'N/A') AS finished,, g.is_active", FALSE);
        $this->db->from('games g');
        $this->db->join('configs c', 'c.config_id = g.config', 'INNER');
        $this->db->join('users u1', 'u1.user_id = g.player_1', 'INNER');
        $this->db->join('users u2', 'u2.user_id = g.player_2', 'LEFT OUTER');
        
        if ($search) {
            if (!empty($search->player1))  $this->db->like('player1', $search->player1);
            if (!empty($search->player2))  $this->db->like('player2', $search->player2);
            if (!empty($search->started))  $this->db->where('started >= ', $search->started);
            if (!empty($search->finished)) $this->db->where('finished <= ', $search->finished);
            if (!empty($search->is_active) || $search->is_active == 0) $this->db->where('is_active', $search->is_active);
            
            if (!empty($search->lead_winner)) {
                $this->db->group_by('game_id');
                $this->db->having("lead_winner LIKE '%".$search->lead_winner."%'");
            }
        }
        
        $this->db->limit($limit, $start);        
        
        $qry = $this->db->get();
        $games = $qry->result_object();
        
        return $games;
    }
    
    public function count_games() {
        return $this->db->count_all('games');
    }
}
