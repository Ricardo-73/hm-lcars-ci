<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of newPHPClass
 *
 * @author ricardo
 */
class ElementModel extends CI_Model {
    
    public function element($id) {
        $this->db->select('element_id, element, is_active');
        $this->db->from('elements');
        $this->db->where('element_id', $id);
        
        $qry = $this->db->get();
        $element = $qry->first_row();
        
        return $element;
    }
    
    public function create($element) {
        $id = $this->db->insert('elements', $element);
        
        if ($id) {
            $id = $this->db->insert_id();
            return $id;
        }
        else
            return FALSE;
    }
    
    public function delete($element) {
        $del = $this->db->delete('elements', array('element_id' => $element));
        return $del;
    }
    
    public function update($element) {
        $this->db->set($element);
        $this->db->where('element_id', $element['element_id']);
        $upd = $this->db->update('elements');
        
        return $upd;
    }
    
    public function get_elements($limit, $start, $search = NULL) {
        $this->db->select('element_id, element, created, is_active');
        $this->db->from('elements');
        
        if ($search) {
            if (!empty($search->element))    $this->db->like('element', $search->element);
            if (!empty($search->created[0])) $this->db->where('created >= ', $search->created[0]);
            if (!empty($search->created[1])) $this->db->where('created <= ', $search->created[1]);
            if (!empty($search->is_active) || $search->is_active == 0) $this->db->where('is_active', $search->is_active);
        }
        
        $this->db->limit($limit, $start);
        
        $qry = $this->db->get();
        $elements = $qry->result_object();
        
        return $elements;
    }
    
    public function count_elements() {
        return $this->db->count_all('elements');
    }
}
