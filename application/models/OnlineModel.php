<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Online
 *
 * @author ricardo
 */
class Online extends CI_Model {
    
    public function online() {
        $this->db->select('user_id, username');
        $this->db->from('users');
        $this->db->where('is_logged_in', 1);
        
        $qry = $this->db->get();
        
        $online = new \stdClass;
        $online->users = $qry->result_object();
        $online->count = $qry->num_rows();
        
        return $online;
    }
    
    public function is_online($id) {
        $this->db->select('user_id, username');
        $this->db->from('users');
        $this->db->where('is_logged_in', 1);
        $this->db->where('user_id', $id);
        
        $qry = $this->db->get();
        return $qry->num_rows();
    }
}
