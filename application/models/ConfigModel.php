<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConfigModel
 *
 * @author ricardo
 */
class ConfigModel extends CI_Model {
    
    public function config($id) {
        $qry = $this->db->get_where('configs', array('config_id', $id));
        $config = $qry->first_row();
        
        return $config;
    } 
    
    public function create($config) {
        $id = $this->db->insert('configs', $config);
                
        if ($id) {
            $id = $this->db->insert_id();
            return $id;
        }
        else {
            return FALSE;
        }
    }
    
    public function update($config) {
        $this->db->set($config);
        $this->db->where('config_id', $config['config_id']);
        $upd = $this->db->update('configs');
        
        return $upd;
    }
    
    public function delete($config) {
        $del = $this->db->delete('configs', array('config_id' => $config));
        return $del;
    }
    
    public function get_configs() {
        $qry = $this->db->get('configs');
        $configs = $qry->result_object();
        
        return $configs;
    }
    
    public function count_configs() {
        return $this->db->count_all('configs');
    }
}
