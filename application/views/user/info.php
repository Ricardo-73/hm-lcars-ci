<?php $this->load->view('admin/navbar'); ?>

<div class="container users" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <h2 style="color: #FFFFFF">User Information</h2>
    
    <table class="table">
        <tr>
            <th>&nbsp;</th>
            <th>UserID: <?php echo $user->user_id; ?></th>
        </tr>
        <tr>
            <td>Name: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->name; ?>" readonly></td>
        </tr>
        <tr>
            <td>Username: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->username; ?>" readonly></td>
        </tr>
        <tr>
            <td>Games Played: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->games_played; ?>" readonly></td>
        </tr>
        <tr>
            <td>Wins: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->wins; ?>" readonly></td>
        </tr>
        <tr>
            <td>Losses: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->lost; ?>" readonly></td>
        </tr>
        <tr>
            <td>Current Score: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->score; ?>" readonly></td>
        </tr>
        <tr>
            <td>Last Login: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->last_login; ?>" readonly></td>
        </tr>
        <tr>
            <td>Creation Date: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->created; ?>" readonly></td>
        </tr>
        <tr>
            <td>Last Updated: </td>
            <td><input type="text" class="form-control" value="<?php echo $user->updated; ?>" readonly></td>
        </tr>
        <tr>
            <td>Status: </td>
            <td><input type="text" class="form-control" value="<?php echo ($user->is_active) ? "Active" : "Inactive"; ?>" readonly></td>
        </tr>
    </table>
</div>