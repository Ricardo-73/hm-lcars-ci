<?php $this->load->view('game/navbar'); ?>

<div class="container profile" role="main">
    <?php if ($this->session->flashdata('info')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-info" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('info'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('error'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <?php $this->load->view('stardate'); ?>
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2 login-register">
            <?php echo form_open('user/user/profile_action', array('class' => 'form-inline')); ?>
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" name="username" placeholder="Username" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Username'" value="<?php echo (!$user) ? set_value('name') : $user->username; ?>" required>
                </div>
                <div class="clearfix" style="margin-bottom: 10px;"></div>
                
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" placeholder="Your Name" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Your Name'" value="<?php echo (!$user) ? set_value('name') : $user->name; ?>" required>
                </div>
                <div class="clearfix" style="margin-bottom: 10px;"></div>
                
                <div class="form-group">
                    <label for="email">E-Mail:</label>
                    <input type="email" class="form-control" name="email" placeholder="Your E-Mail" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Your E-Mail'" value="<?php echo (!$user) ? set_value('email') : $user->email; ?>" required>
                </div>
                <div class="clearfix" style="margin-bottom: 10px;"></div>
                
                <div class="form-group">
                    <label for="password">New Password:</label>
                    <input type="password" class="form-control" name="password" placeholder="New Password" onfocus="this.placeholder = '';" onblur="this.placeholder = 'New Password'">
                </div>
                <div class="clearfix" style="margin-bottom: 10px;"></div>
                
                <div class="form-group">
                    <label for="confirm">Confirm Password:</label>
                    <input type="password" class="form-control" name="Confirm" placeholder="Confirm Password" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Confirm Password'">
                </div>
                <div class="clearfix" style="margin-bottom: 10px;"></div>
                
                <div class="form-group">
                    <label for="created">Created:</label>
                    <input type="text" class="form-control" value="<?php echo $user->created; ?>" readonly>
                </div>
                <div class="clearfix" style="margin-bottom: 10px;"></div>
                
                <div class="form-group">
                    <label for="updated">Updated:</label>
                    <input type="text" class="form-control" value="<?php echo $user->updated; ?>" readonly>
                </div>
                <div class="clearfix" style="margin-bottom: 10px;"></div>
                
                <button type="reset" class="btn btn-primary buttons">Reset</button>
                <button type="submit" class="btn btn-primary buttons">Save</button>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>