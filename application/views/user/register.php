<div class="vcenter">
    <div class="container" role="main">
        <?php if ($this->session->flashdata('warning')) { ?>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 alert alert-warning" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                    <span><?php echo $this->session->flashdata('warning'); ?></span>
                </div>
            </div>
        <?php } ?>
        
        <?php if (validation_errors()) { ?>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                    <span><?php echo validation_errors(); ?></span>
                </div>
            </div>
        <?php } ?>
        
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 login-register">
                <?php echo form_open('user/user/register_action'); ?>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Your Name" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Your Name'" value="<?php echo set_value('name'); ?>" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Your E-Mail" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Your E-Mail'" value="<?php echo set_value('email'); ?>" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Choose a Username" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Choose a Username'" value="<?php echo set_value('username'); ?>" required>
                    </div><div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Enter a Password" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Enter a Password'" value="<?php echo set_value('password'); ?>" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="confirm" placeholder="Confirm Password" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Confirm Password'" value="<?php echo set_value('confirm'); ?>" required>
                    </div>

                    <button type="submit" class="btn btn-primary buttons">Register</button>
                    <a href="<?php echo site_url('user/user/login'); ?>" class="btn btn-primary buttons" role="button">Login</a>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>