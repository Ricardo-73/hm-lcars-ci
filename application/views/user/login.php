<div class="vcenter">
    <div class="container" role="main">
        <?php if ($this->session->flashdata('warning')) { ?>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 alert alert-warning" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                    <span><?php echo $this->session->flashdata('warning'); ?></span>
                </div>
            </div>
        <?php } ?>
        
        <?php if (validation_errors()) { ?>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                    <span><?php echo validation_errors(); ?></span>
                </div>
            </div>
        <?php } ?>
        
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 login-register translucent">
                <?php echo form_open("user/user/login_action"); ?>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Username" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Username'" value="<?php echo set_value('username'); ?>" maxlength="12" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Password'" value="<?php echo set_value('password'); ?>" maxlength="20" required>
                    </div>

                    <button type="submit" class="btn btn-primary buttons">Login</button>
                    <a href="<?php echo site_url('user/user/register'); ?>" class="btn btn-primary buttons" role="button">Register</a>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>