<?php $this->load->view('game/navbar.php'); ?>

<div class="container statistics" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <h2 style="color: #FFFFFF">Player Statistics</h2>
    
    <table class="table">
        <tr>
            <td>Games Played: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->games_played; ?>" readonly></td>
        </tr>
        <tr>
            <td>Wins: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->wins; ?>" readonly></td>
        </tr>
        <tr>
            <td>Losses: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->lost; ?>" readonly></td>
        </tr>
        <tr>
            <td>Current Score: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->score; ?>" readonly></td>
        </tr>
        <tr>
            <td>Highest Score: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->highest; ?>" readonly></td>
        </tr>
        <tr>
            <td>Lowest Score: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->lowest; ?>" readonly></td>
        </tr>
        <tr>
            <td>Average Score: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->average; ?>" readonly></td>
        </tr>
        <tr>
            <td>Win Ratio: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->wrate; ?>" readonly></td>
        </tr>
        
        <tr>
            <td>Loss Ratio: </td>
            <td><input type="text" class="form-control" value="<?php echo $stats->lrate; ?>" readonly></td>
        </tr>
    </table>
</div>
