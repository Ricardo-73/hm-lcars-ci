<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo site_url('main/home'); ?>">STAR TREK - The Imprisoned Man</a>
        </div>
        
        <ul class="nav navbar-nav">
            <li class="active">
                <button class="btn btn-default navbar-btn menu-buttons" <?php if ($hasPending) { ?> disabled <?php } else { ?> onclick="location.href='<?php echo site_url('game/game/new'); ?>'" <?php } ?>>New Game</button>
            </li>
            <li>
                <button class="btn btn-default navbar-btn menu-buttons" onclick="location.href='<?php echo site_url('game/game/load'); ?>'">Load Game</button>
            </li>
            <li>
                <button class="btn btn-default navbar-btn menu-buttons" onclick="location.href='<?php echo site_url('user/stats/statistics'); ?>'">Statistics</button>
            </li>
            <li>
                <button class="btn btn-default navbar-btn menu-buttons" onclick="location.href='<?php echo site_url('user/user/profile'); ?>'">My Profile</button>
            </li>
        </ul>
        
        <a href="<?php echo site_url('user/user/logout_action'); ?>" class="btn btn-default navbar-btn" role="button">Logout</a>
    </div>
</nav>
