<?php $this->load->view('admin/navbar'); ?>

<div class="container users" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <h2 style="color: #FFFFFF">Game Information</h2>
    
    <table class="table">
        <tr>
            <th>&nbsp;</th>
            <th>GameID: <?php echo $game->user_id; ?></th>
        </tr>
        <tr>
            <td>Config: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->config; ?>" readonly></td>
        </tr>
        <tr>
            <td>Player 1: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->player_1; ?>" readonly></td>
        </tr>
        <tr>
            <td>Player 2: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->player_2; ?>" readonly></td>
        </tr>
        <tr>
            <td>Lead / Winner: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->lead_winner; ?>" readonly></td>
        </tr>
        <tr>
            <td>Player 1 Score: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->player1_score; ?>" readonly></td>
        </tr>
        <tr>
            <td>Player 2 Score: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->player2_score; ?>" readonly></td>
        </tr>
        <tr>
            <td>Started: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->started; ?>" readonly></td>
        </tr>
        <tr>
            <td>Finished: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->finished; ?>" readonly></td>
        </tr>
        <tr>
            <td>Elapsed Time: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->elapsed; ?>" readonly></td>
        </tr>
        <tr>
            <td>Last Saved: </td>
            <td><input type="text" class="form-control" value="<?php echo $game->last_saved; ?>" readonly></td>
        </tr>
        <tr>
            <td>Status: </td>
            <td><input type="text" class="form-control" value="<?php echo ($game->is_active) ? "In Progress" : "Finished"; ?>" readonly></td>
        </tr>
    </table>
</div>