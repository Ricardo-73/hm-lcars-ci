<?php $this->load->view('game/navbar.php'); ?>

<div class="container welcome" role="main">
    <?php if ($this->session->flashdata('info')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-info" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('info'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <?php $this->load->view('stardate'); ?>
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center" style="color: #FFFFFF;">
            <h1>Welcome aboard.</h1>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="font-size: 24px; color: #FFFFFF;">            
                &nbsp;This is the 23rd Century. Capital punishment, a grimm reminder of Humanity's brutal and uncivilized times, is abolished.
                <br />
                Well... There are some exceptions. But in essencial, it is abolished. And with it, so is death by hanging. 
            </p>
            
            <p style="word-wrap: normal;">
                &nbsp;The United Federation of Planets will continue its endeavors to completely ban death penalty for good. That will become a reality in mid 24th Century.
                But time-travel or futurology is not the subject of this game, although they could be of interest. You have been summoned to be a part of that endeavour.
            </p>
            
            <p style="word-wrap: normal; font-size">
                &nbsp;<span style="font-size: 30px;"> </span> 
            </p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="color: #FFFFFF;">
            <p style="word-wrap: normal; font-size: 34px;">
                Your Mission: To boldly guess the words, phrases or sentences that no one... Ok, that someone might have guessed before!
            </p>
            
            <p style="word-wrap: normal; font-size: 24px;">
                Commit yourself to that, and you'll avoid being arrested.
            </p>
            
            <blockquote style="float: right; font-style: italic;">
                <p>"Capital punishment, in our world, is no longer considered a justifiable deterrent."</p>
                <footer>Jean-Luc Picard, 2364</footer>
            </blockquote>
        </div>
    </div>
</div>

