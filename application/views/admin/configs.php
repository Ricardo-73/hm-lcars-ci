<?php $this->load->view('admin/navbar'); ?>

<div class="container configs" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <?php if ($this->session->flashdata('info')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-info" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('info'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <div class="row confirm" style="display: none;">
        <div class="col-sm-4 col-sm-offset-4 alert alert-confirm">
            <span>Sir, cinfiguration elimination is not reversible. Shall I proceed?</span>
            
            <a href="<?php echo site_url('admin/config/'); ?>" class="btn btn-default">Yes</a>
            <button class="btn btn-default" onclick="$('.row.confirm').fadeOut('slow');">No</button>
        </div>
    </div>
    
    <h2 style="color: #FFFFFF">Configurations</h2>
    
    <table class="table table-hover" style="color: #CCCCCC; font-size: 24px;">
        <tr>
            <th>ID</th>
            <th>Hit</th>
            <th>Miss</th>
            <th>Attempts</th>
            <th>Timer</th>
            <th>Created</th>
            <th>Active</th>
            <th>&nbsp;</th>
            <th>
                <?php if (count($configs) < MAX_CONFIG) { ?>
                    <a href="<?php echo site_url('admin/config/config'); ?>" class="btn btn-default">New</a>
                <?php } ?>
            </th>
        </tr>
        
        <?php if ($configs) {
            foreach ($configs as $value) { ?>
                <tr>
                    <td><?php echo $value->config_id; ?></td>
                    <td><?php echo $value->score_hit; ?></td>
                    <td><?php echo $value->score_miss; ?></td>
                    <td><?php echo $value->attempts; ?></td>
                    <td><?php echo $value->timer; ?></td>
                    <td><?php echo $value->created; ?></td>
                    <td><?php echo $value->is_active ? 'Yes' : 'No'; ?></td>
                    <td><a href="<?php echo site_url('admin/config/config/'.$value->config_id); ?>" class="btn btn-default">Edit</a></td>
                    <td>
                        <?php if (!$value->is_active) { ?>
                            <button class="btn btn-warning" onclick="setDelete('config', '<?php echo $value->config_id; ?>')">Delete</button></td>
                        <?php } ?>
                </tr>
        <?php }
        } 
        ?>
    </table>
    
    <?php if (!$configs) { ?>
        <div style="color: #FFFFFF; font-size: 24px;">Sorry, Sir. No configuration(s) found.</div>
    <?php } ?>
</div>
