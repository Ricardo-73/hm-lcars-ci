<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?php echo form_open('admin/user/get_users'); ?>
            <ul class="nav navbar-nav">
                <li>
                    <div class="form-group">
                        <input class="form-control" name="id" placeholder="User ID" onfocus="this.placeholder = '';" onblur="this.placeholder = 'User ID'" value="<?php echo set_value('user_id'); ?>">
                        <?php echo form_hidden('search', 1); ?>
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" name="username" placeholder="Username" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Username'" value="<?php echo set_value('username'); ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" name="name" placeholder="Name" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Name'" value="<?php echo set_value('name'); ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control date" id="idate" name="created[]" placeholder="Creation Date From" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Creation Date From'" value="<?php echo set_value('created')[0]; ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control date" id="fdate" name="created[]" placeholder="Creation Date To" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Creation Date To'" value="<?php echo set_value('created')[1]; ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <label for="active" style="color: #FFFFFF; font-size: 24px;">Active?</label>
                        <select class="form-control" id="active" name="active" style="color: #000000; font-size: 24px;">
                            <option value="0" <?php if (!set_value('is_active')) { ?> selected <?php } ?>>No</option>
                            <option value="1" <?php if (!set_value('is_active')) { ?> selected <?php } ?>>Yes</option>
                        </select>
                    </div>
                </li>
            </ul>
            
            <ul class="nav navbar-nav">
                <li>
                    <button type="reset" class="btn btn-primary buttons">Reset Search</button>
                </li>                
                <li>
                    <button type="submit" class="btn btn-primary buttons">Submit Search</button>
                </li>
            </ul>
        <?php echo form_close(); ?>
    </div>
</div>
