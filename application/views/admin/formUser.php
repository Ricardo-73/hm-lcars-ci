<?php $this->load->view('admin/navbar'); ?>

<div class="container users" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <?php if ($this->session->flashdata('warning')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-warning" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('warning'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('error'); ?></span>
            </div>
        </div>
    <?php } ?>

    <?php if (validation_errors()) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo validation_errors(); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <h2 style="color: #FFFFFF"><?php if (!$user) { ?> New <?php } else { ?> Edit <?php } ?> User </h2>
    
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 new-user">
            <?php echo (!$user) ? form_open('admin/user/set_user') : form_open('admin/user/upd_user'); ?>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Your Name" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Your Name'" value="<?php echo (!$user) ? set_value('name') : $user->name; ?>" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Your E-Mail" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Your E-Mail'" value="<?php echo (!$user) ? set_value('email') : $user->email; ?>" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Choose a Username" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Choose a Username'" value="<?php echo (!$user) ? set_value('username') : $user->username; ?>" required>
                </div>
                
                <?php if ($user) { ?>
                    <div class="form-group">
                        <label for="active" style="color: #FFFFFF; font-size: 24px;">Active?</label>
                        <select class="form-control" id="active" name="active" style="color: #000000; font-size: 24px;">
                            <option value="0" <?php if (!$user->is_active) { ?> selected <?php } ?>>No</option>
                            <option value="1" <?php if ($user->is_active) { ?> selected <?php } ?>>Yes</option>
                        </select>
                    </div>
            
                    <div class="form-group">
                        <label for="reset" style="color: #FFFFFF; font-size: 24px;">Reset Password?</label>
                        <select class="form-control" id="reset" name="reset" style="color: #000000; font-size: 24px;">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                        <?php echo form_hidden('user_id', $user->user_id); ?>
                    </div>
                <?php } ?>
            
                <button type="reset" class="btn btn-primary buttons">Reset</button>
                <button type="submit" class="btn btn-primary buttons">Save</button>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>