<?php $this->load->view('admin/navbar'); ?>

<div class="container elements" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <?php if ($this->session->flashdata('warning')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-warning" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('warning'); ?></span>
            </div>
        </div>
    <?php } ?>

    <?php if ($this->session->flashdata('error')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('error'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <?php if (validation_errors()) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo validation_errors(); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <h2 style="color: #FFFFFF"><?php if (!$config) { ?> New <?php } else { ?> Edit <?php } ?> Configuration </h2>
    
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 new-config">
            <?php echo (!$config) ? form_open('admin/admin/set_config') : form_open('admin/admin/upd_config'); ?>
                <div class="form-group">
                    <input type="number" class="form-control" name="hit" placeholder="Hit Score" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Hit Score'" value="<?php echo (!$config) ? set_value('score_hit') : $config->score_hit; ?>" required>
                </div>
            
                <div class="form-group">
                    <input type="number" class="form-control" name="miss" placeholder="Miss Score" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Miss Score'" value="<?php echo (!$config) ? set_value('score_miss') : $config->score_miss; ?>" required>
                </div>
            
                <div class="form-group">
                    <input type="number" class="form-control" id="attempts" name="attempts" placeholder="Attempts" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Attempts'" onkeyup="resetConfigXOR(this);" value="<?php echo (!$config) ? set_value('attempts') : $config->attempts; ?>">
                </div>
            
                <div class="form-group">
                    <input type="number" class="form-control" id="timer" name="timer" placeholder="Timer Value" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Timer Value'" onkeyup="resetConfigXOR(this); " value="<?php echo (!$config) ? set_value('timer') : $config->timer; ?>">
                </div>
            
                <?php if (!$config || $count > 1) { ?>
                    <div class="form-group">
                        <label for="active" style="color: #FFFFFF; font-size: 24px;">Make this configuration active?</label>
                        <select class="form-control" id="active" name="active" style="color: #000000; font-size: 24px;">
                            <option value="0" <?php if (!$config->is_active) { ?> selected <?php } ?>>No</option>
                            <option value="1" <?php if ($config->is_active) { ?> selected <?php } ?>>Yes</option>
                        </select>

                        <?php if ($config) echo form_hidden('config_id', $config->config_id); ?>
                    </div>
                <?php } ?>
                
                <button type="reset" class="btn btn-primary buttons">Reset</button>
                <button type="submit" class="btn btn-primary buttons">Save</button>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
