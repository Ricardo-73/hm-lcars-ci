<?php $this->load->view('admin/navbar'); ?>

<div class="container games" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <?php if ($this->session->flashdata('info')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-info" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('info'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <div class="row confirm" style="display: none;">
        <div class="col-sm-4 col-sm-offset-4 alert alert-confirm">
            <span>Sir, game elimination is not reversible. Shall I proceed?</span>
            
            <a href="<?php echo site_url('admin/game/'); ?>" class="btn btn-default">Yes</a>
            <button class="btn btn-default" onclick="$('.row.confirm').fadeOut('slow');">No</button>
        </div>
    </div>
    
    <?php $this->load->view('admin/gameFilter'); ?>
    
    <h2 style="color: #FFFFFF">Games</h2>
    
    <table class="table table-hover" style="color: #CCCCCC; font-size: 24px;">
        <tr>
            <th>ID</th>
            <th>Config</th>
            <th>Player 1</th>
            <th>Player 2</th>
            <th>Lead / Winner</th>
            <th>Started</th>
            <th>Finished</th>
            <th>Active</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>

        <?php if ($games) { 
            foreach ($games as $value) { ?>
                <tr>
                    <td><?php echo $value->game_id; ?></td>
                    <td><?php echo $value->name; ?></td>
                    <td><?php echo $value->player_1; ?></td>
                    <td><?php echo $value->player_2; ?></td>
                    <td><?php echo $value->lead_winner; ?></td>
                    <td><?php echo $value->started; ?></td>
                    <td><?php echo $value->finished; ?></td>
                    <td><?php echo $value->is_active; ?></td>
                    <td><a href="<?php echo site_url('admin/game/view/'.$value->game_id); ?>" class="btn btn-default">View</a></td>
                    <td><button class="btn btn-warning" onclick="setDelete('game', '<?php echo $value->user_id; ?>')">Delete</button></td>
                </tr>
        <?php } 
        }
        ?>
    </table>
    
    <?php if (!$games) { ?>
        <div style="color: #FFFFFF; font-size: 24px;">Sorry, Sir. No game(s) found.</div>
    <?php
    } 
    else { ?>    
        <div style="color: #FFFFFF; font-size: 24px;"><?php echo $links; ?></div>
    <?php } ?>
</div>
