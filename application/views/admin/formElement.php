<?php $this->load->view('admin/navbar'); ?>

<div class="container elements" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <?php if ($this->session->flashdata('warning')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-warning" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('warning'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('error'); ?></span>
            </div>
        </div>
    <?php } ?>

    <?php if (validation_errors()) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-error" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo validation_errors(); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <h2 style="color: #FFFFFF"><?php if (!$element) { ?> New <?php } else { ?> Edit <?php } ?> Element </h2>
    
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 new-element">
            <?php echo (!$element) ? form_open('admin/element/set_element') : form_open('admin/element/upd_element'); ?>
                <div class="form-group">
                    <input type="text" class="form-control" name="element" placeholder="New Element" onfocus="this.placeholder = '';" onblur="this.placeholder = 'New Element'" value="<?php echo (!$element) ? set_value('element') : $element->element; ?>" required>
                </div>
            
                <?php if ($element) { ?>
                    <div class="form-group">
                        <label for="active" style="color: #FFFFFF; font-size: 24px;">Active?</label>
                        <select class="form-control" id="active" name="active" style="color: #000000; font-size: 24px;">
                            <option value="0" <?php if (!$element->is_active) { ?> selected <?php } ?>>No</option>
                            <option value="1" <?php if ($element->is_active) { ?> selected <?php } ?>>Yes</option>
                        </select>
                        <?php echo form_hidden('element_id', $element->element_id); ?>
                    </div>
                <?php } ?>
                
                <button type="reset" class="btn btn-primary buttons">Reset</button>
                <button type="submit" class="btn btn-primary buttons">Save</button>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>