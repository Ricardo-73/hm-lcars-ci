<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?php echo form_open('admin/game/get_games'); ?>
            <ul class="nav navbar-nav">
                <li>
                    <div class="form-group">
                        
                        <?php echo form_hidden('search', 1); ?>
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" name="player1" placeholder="Player 1" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Player 1'" value="<?php echo set_value('player_1'); ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" name="player2" placeholder="Player 2" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Player 2'" value="<?php echo set_value('player_2'); ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" name="lead_winner" placeholder="Lead / Winner" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Lead / Winner'" value="<?php echo set_value('lead_winner'); ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control date" id="idate" name="started" placeholder="Started After" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Started After'" value="<?php echo set_value('started'); ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control date" id="fdate" name="finished" placeholder="Finished Before" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Finished Before'" value="<?php echo set_value('finished'); ?>">
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <label for="active" style="color: #FFFFFF; font-size: 24px;">Active?</label>
                        <select class="form-control" id="active" name="active" style="color: #000000; font-size: 24px;">
                            <option value="0" <?php if (!set_value('is_active')) { ?> selected <?php } ?>>No</option>
                            <option value="1" <?php if (set_value('is_active')) { ?> selected <?php } ?>>Yes</option>
                        </select>
                    </div>
                </li>
            </ul>
        
            <ul class="nav navbar-nav">
                <li>
                    <button type="reset" class="btn btn-primary buttons">Reset Search</button>
                </li>                
                <li>
                    <button type="submit" class="btn btn-primary buttons">Submit Search</button>
                </li>
            </ul>
        <?php echo form_close(); ?>
    </div>
</div>