<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo site_url('main/home'); ?>">STAR TREK - The Imprisoned Man</a>
        </div>
        
        <ul class="nav navbar-nav">
            <li>
                <button class="btn btn-default navbar-btn menu-buttons" onclick="location.href='<?php echo site_url('admin/admin'); ?>'">Main</button>
            </li>
            <li>
                <button class="btn btn-default navbar-btn menu-buttons" onclick="location.href='<?php echo site_url('admin/user/get_users'); ?>'">Users</button>
            </li>
            <li>
                <button class="btn btn-default navbar-btn menu-buttons" onclick="location.href='<?php echo site_url('admin/element/get_elements'); ?>'">Game Elements</button>
            </li>
            <li>
                <button class="btn btn-default navbar-btn menu-buttons" onclick="location.href='<?php echo site_url('admin/config/get_configs'); ?>'">Configurations</button>
            </li>
            <li>
                <button class="btn btn-default navbar-btn menu-buttons" onclick="location.href='<?php echo site_url('admin/game/get_games'); ?>'">Games</button>
            </li>
        </ul>
        
        <a href="<?php echo site_url('user/user/logout_action'); ?>" class="btn btn-default navbar-btn logout" role="button">Logout</a>
    </div>
</nav>