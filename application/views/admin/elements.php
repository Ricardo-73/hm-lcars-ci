<?php $this->load->view('admin/navbar'); ?>

<div class="container elements" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <?php if ($this->session->flashdata('info')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-info" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('info'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <div class="row confirm" style="display: none;">
        <div class="col-sm-4 col-sm-offset-4 alert alert-confirm">
            <span>Sir, game element elimination is not reversible. Shall I proceed?</span>
            
            <a href="<?php echo site_url('admin/element/'); ?>" class="btn btn-default">Yes</a>
            <button class="btn btn-default" onclick="$('.row.confirm').fadeOut('slow');">No</button>
        </div>
    </div>
    
    <?php $this->load->view('admin/elementFilter'); ?>
    
    <h2 style="color: #FFFFFF">Elements</h2>
    
    <table class="table table-hover" style="color: #CCCCCC; font-size: 24px;">
        <tr>
            <th>ID</th>
            <th>Element</th>
            <th>Created</th>
            <th>Active</th>
            <th>&nbsp;</th>
            <th><a href="<?php echo site_url('admin/element/element'); ?>" class="btn btn-default">New</a></th>
        </tr>

        <?php if ($elements) { 
            foreach ($elements as $value) { ?>
                <tr>
                    <td><?php echo $value->element_id; ?></td>
                    <td><?php echo $value->element; ?></td>
                    <td><?php echo $value->created; ?></td>
                    <td><?php echo $value->is_active ? 'Yes' : 'No'; ?></td>
                    <td><a href="<?php echo site_url('admin/element/element/'.$value->element_id); ?>" class="btn btn-default">Edit</a></td>
                    <td><button class="btn btn-warning" onclick="setDelete('element', '<?php echo $value->element_id; ?>')">Delete</button></td>
                </tr>
        <?php } 
        }
        ?>
    </table>
    
    <?php if (!$elements) { ?>
        <div style="color: #FFFFFF; font-size: 24px;">Sorry, Sir. No element(s) found.</div>
    <?php } ?>
        
    <div style="color: #FFFFFF; font-size: 24px;"><?php echo $links; ?></div>
</div>