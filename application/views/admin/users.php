<?php $this->load->view('admin/navbar'); ?>

<div class="container users" role="main">
    <?php $this->load->view('stardate'); ?>
    
    <?php if ($this->session->flashdata('info')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-info" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('info'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <div class="row confirm" style="display: none;">
        <div class="col-sm-4 col-sm-offset-4 alert alert-confirm">
            <span>Sir, user elimination is not reversible. Shall I proceed?</span>
            
            <a href="<?php echo site_url('admin/user/'); ?>" class="btn btn-default">Yes</a>
            <button class="btn btn-default" onclick="$('.row.confirm').fadeOut('slow');">No</button>
        </div>
    </div>
    
    <?php $this->load->view('admin/userFilter'); ?>
    
    <h2 style="color: #FFFFFF">Users</h2>
    
    <table class="table table-hover" style="color: #CCCCCC; font-size: 24px;">
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Name</th>
            <th>Created</th>
            <th>Score</th>
            <th>Active</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th><a href="<?php echo site_url('admin/user/user'); ?>" class="btn btn-default">New</a></th>
        </tr>

        <?php if ($users) { 
            foreach ($users as $value) { ?>
                <tr>
                    <td><?php echo $value->user_id; ?></td>
                    <td><?php echo $value->username; ?></td>
                    <td><?php echo $value->name; ?></td>
                    <td><?php echo $value->created; ?></td>
                    <td><?php echo $value->score; ?></td>
                    <td><?php echo $value->is_active ? 'Yes' : 'No'; ?></td>
                    <td><a href="<?php echo site_url('admin/user/info/'.$value->user_id); ?>" class="btn btn-default">Info</a></td>
                                        
                    <?php if (!$value->is_admin) { ?>
                        <td><a href="<?php echo site_url('admin/user/user/'.$value->user_id); ?>" class="btn btn-default">Edit</a></td>
                        <td><button class="btn btn-warning" onclick="setDelete('user', '<?php echo $value->user_id; ?>')">Delete</button></td>
                    <?php
                    }
                    else { ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php } ?>
                </tr>
        <?php } 
        }
        ?>
    </table>
    
    <?php if (!$users) { ?>
        <div style="color: #FFFFFF; font-size: 24px;">Sorry, Sir. No user(s) found.</div>
    <?php
    } 
    else { ?>    
        <div style="color: #FFFFFF; font-size: 24px;"><?php echo $links; ?></div>
    <?php } ?>
</div>
