<?php $this->load->view('admin/navbar'); ?>

<div class="container welcome" role="main">
    <?php if ($this->session->flashdata('info')) { ?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 alert alert-info" onclick="$(this).fadeOut('slow', function() { $(this).remove(); } );">
                <span><?php echo $this->session->flashdata('info'); ?></span>
            </div>
        </div>
    <?php } ?>
    
    <?php $this->load->view('stardate'); ?>
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            <h1>
                <span>CAPTAIN ON DECK!</span>
                <div style="clear: both;"></div>
                <small>Welcome aboard, Sir. Your orders?</small>
            </h1>
        </div>
    </div>
</div>