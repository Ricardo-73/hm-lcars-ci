<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author ricardo
 */
class User extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $data['title'] = "Login";
        
        $this->load->view('header', $data);
        $this->load->model('userModel');
    }
    
    public function index() {
        redirect('main');
    }
    
    public function login() {
        $data['title'] = "Login";
        
        $this->load->view('header', $data);
        $this->load->view('user/login');
        $this->load->view('footer');
    }
        
    public function login_action() {
        $rules = array(
            array(
                'field' => "username", 
                'label' => "Username", 
                'rules' => "trim|required", 
                array(
                    'required'      => "Please fill in the %s field.",
                )),
            array(
                'field' => "password",
                'label' => "Password",
                'rules' => "trim|required",
                array(
                    'required'      => "Please fill in the %s field.",
                )
            ),
        );
    
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $user = $this->input->post('username');
            $pass = $this->input->post('password');
            
            $user = $this->userModel->login($user, $pass); 
            
            if ($user) {
                $this->session->set_flashdata('info', "User login successfull.");
                $this->session->set_userdata($user);
                redirect('main');
            }
            else {
                $this->session->set_flashdata('warning', "Unknown user credentials.");
                redirect('user/user/login');
            }
            
        }
        else {
            $this->load->view('user/login');
            $this->load->view('footer');
        }
    }    
    
    public function logout_action() {
        $online = $this->session->tempdata('online');
        $user['is_logged_in'] = 0;
        
        $this->userModel->logout($user);
        $this->session->sess_destroy();
        
        $this->session->set_tempdata($online, CRON_DELTA);
        redirect ('user/user/login', 'refresh');
    }
    
    public function register() {
        $data['title'] = "Register";
        
        $this->load->view('header', $data);
        $this->load->view('user/register');
        $this->load->view('footer');
    }
        
    public function register_action() {  
        $rules = array(
            array(
                'field' => "name",
                'label' => "Your Name",
                'rules' => "required",
                'errors' => array(
                    'required' => "Please fill in %s."
                )
            ),
            array(
                'field' => "email",
                'label' => "E-Mail",
                'rules' => "required|valid_email",
                'errors' => array(
                    'required'    => "Please fill in the %s field.",
                    'valid_email' => "Please enter a valid %s address."
                )
            ),
            array(
                'field' => "username",
                'label' => "Username",
                'rules' => "trim|required|min_length[5]|max_length[12]|is_unique[users.username]",
                'errors' => array(
                    'required'   => "Please fill in the %s field.",
                    'min_length' => "The %s must have at least 5 chars.",
                    'max_length' => "The %s must have at most 12 chars.",
                    'is_unique'  => "The chosen %s is already taken."
                )
            ),
            array(
                'field' => "password",
                'label' => "Password",
                'rules' => "required|min_length[8]",
                'errors' => array(
                    'required'   => "trim|Please fill in the %s field.",
                    'min_length' => "The %s must have at least 8 chars."
                )
            ),
            array(
                'field' => "confirm",
                'label' => "Confirm Password",
                'rules' => "trim|required|matches[password]",
                'errors' => array(
                    'required' => "Please fill in the %s field.",
                    'matches'  => "Passwords do not match."
                )
            ),
        );    
        
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $register = array(
                'name'     => $this->input->post('name'),
                'email'    => $this->input->post('email'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
            );
            
            $user = $this->userModel->create($register);
            
            if ($user) {
                $this->session->set_flashdata('info', "User registration successfull.");
                $this->session->set_userdata($user);
                redirect ('main');
            }
            else {
                $this->session->set_flashdata('error', "Sorry, something went wrong...");
                redirect('user/user/register');
            }
        }
        else {
            $this->load->view('user/register');
            $this->load->view('footer');
        }
    }
    
    public function profile() {
        if (!$this->session->userdata('user_id'))
            redirect ('user/user/login');
        
        $this->load->library('Calculations');
        
        $data['title'] = "";
        
        $data['todayDates'] = Calculations::calc_stardate(date('Y-m-d H:i:s'));
        $data['loginDates'] = Calculations::calc_stardate($this->sessionId['last_login']);
        $data['sessionId']  = $this->session->userdata;
        $data['user']       = $this->userModel->info($data['sessionId']['user_id']);
        
        $this->load->view('header', $data);
        $this->load->view('user/profile');
        $this->load->view('footer');
    }
    
    public function profile_save() {
        $rules = array(
            array(
                'field' => "name",
                'label' => "Your Name",
                'rules' => "required",
                array(
                    'required' => "Please fill in %s."
                )
            ),
            array(
                'field' => "email",
                'label' => "E-Mail",
                'rules' => "required|valid_email",
                array(
                    'required'    => "Please fill in the %s field.",
                    'valid_email' => "Please enter a valid %s address."
                )
            ),
            array(
                'field' => "username",
                'label' => "Username",
                'rules' => "trim|required|min_length[5]|max_length[12]|is_unique[users.username]",
                array(
                    'required'   => "Please fill in the %s field.",
                    'min_length' => "The %s must have at least 5 chars.",
                    'max_length' => "The %s must have at most 12 chars.",
                    'is_unique'  => "The chosen %s is already taken."
                )
            ),
        );
        
        if ($this->input->post('password') || $this->input->post('confirm')) {
            $rules[] = array(
                            'field' => "password",
                            'label' => "Password",
                            'rules' => "trim|min_length[8]",
                            array(
                                'min_length' => "The %s must have at least 8 chars."
                            )
                        );
            
            $rules[] = array(
                            'field' => "confirm",
                            'label' => "Confirm Password",
                            'rules' => "trim|matches[password]",
                            array(
                                'required' => "Please fill in the %s field.",
                                'matches'  => "Passwords do not match."
                            )
                        );
        }
        
        $user = $this->userModel->user($this->session->userdata['user_id']);
        
        if ($user->username != $this->input->post('username')) {
            $rules[2]['rules'] .= "|is_unique[users.username]";
            $rules[2]['errors']['is_unique'] = "The chosen %s is already taken.";
        }
        
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $user = array(
                            'username' => $this->input->post('username'),
                            'name'     => $this->input->post('name'),
                            'email'    => $this->input->post('email'),
                        ); 
            
            if ($this->input->post('password'))
                $user['password'] = $this->input->post('password');
            
            $user = $this->userModel->update($user);
            
            if ($user) {
                $this->session->set_flashdata('info', 'Your profile was updated successfully.');
            }
            else {
                $this->session->set_flashdata('error', 'Sorry, an error has occurred while updating your profile.');
            }
            
            redirect('user/user/profile');
        }
        else {
            redirect('user/user/profile');
        }
    }
}
