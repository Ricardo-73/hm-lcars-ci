<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Stats
 *
 * @author ricardo
 */
class Stats extends CI_Controller {
    private $data;
    
    public function __construct() {
        parent::__construct();
        
        if (!$this->session->userdata('user_id'))
            redirect ('user/user/login');
        
        $this->load->model('userModel');
        $this->load->library('Calculations');
        
        $this->data['title'] = " Player Statistics"; 
        $this->data['todayDates'] = Calculations::calc_stardate(date('Y-m-d H:i:s'));
        $this->data['loginDates'] = Calculations::calc_stardate($this->sessionId['last_login']);
        $this->data['sessionId']  = $this->session->userdata();
        
        $this->load->view('header', $this->data);
    }
    
    public function statistics() {
        $info = $this->userModel->info($this->session->userdata('user_id'));
        
        $data['stats'] = new \stdClass;
        
        $data['stats']->games_played = $info->games_played;
        $data['stats']->wins  = $info->wins;
        $data['stats']->lost  = $info->lost;
        $data['stats']->score = $info->score;
        
        if (!$info->games_played)
            $info->games_played = 1;
                
        $data['stats']->wrate = $info->wins / $info->games_played;
        $data['stats']->lrate = $info->lost / $info->games_played;
        
        $stats = $this->userModel->statistics($this->session->userdata('user_id'));
        
        $data['stats']->highest = $stats->highest;
        $data['stats']->lowest  = $stats->lowest;
        $data['stats']->average = round($stats->average, 2);
                
        $this->load->view('user/stats', $data);
        $this->load->view('footer');
    }
}
