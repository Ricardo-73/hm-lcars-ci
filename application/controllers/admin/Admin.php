<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author ricardo
 */
class Admin extends CI_Controller {
    protected $sessionId;
    protected $data;
    
    public $todayDates;
    public $loginDates;
    
    public function __construct() {
        parent::__construct();
        
        if (!$this->session->userdata('user_id'))
            redirect ('user/user/login');
        
        if (!$this->session->userdata('is_admin'))
            redirect('main');
        
        $this->sessionId = $this->session->userdata();
        $this->load->library('Calculations');
        
        $this->data['title'] = " Administration";        
        $this->data['todayDates'] = Calculations::calc_stardate(date('Y-m-d H:i:s'));
        $this->data['loginDates'] = Calculations::calc_stardate($this->sessionId['last_login']);
        $this->data['sessionId']  = $this->sessionId;
        
        $this->load->view('header', $this->data);
    }
    
    public function index() {
        redirect('main');
    }
}
