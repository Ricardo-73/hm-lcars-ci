<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once "Admin.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author ricardo
 */
class User extends Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('userModel');
    }
    
    public function user() { 
        $user = $this->uri->segment(4);
        
        if ($user && $user == 1) // Admin user
            redirect('admin/user/get_users');
        
        if ($user) 
            $data['user'] = $this->userModel->user($user);
        
        if (empty($data['user'])) {
            $this->session->set_flashdata('info', 'Sorry Sir, unable to find that user.');
            redirect('admin/user/get_users');
        }
        
        $data['todayDates'] = $this->data['todayDates'];
        $data['loginDates'] = $this->data['loginDates'];
        
        $this->load->view('admin/formUser', $data);
        $this->load->view('footer');
    }
    
    public function info() {
        $user = $this->uri->segment(4);
        
        if ($user) 
            $data['user'] = $this->userModel->info($user);
        
        if (empty($data['user'])) {
            $this->session->set_flashdata('info', 'Sorry Sir, unable to find that user.');
            redirect('admin/user/get_users');
        }
        
        $data['todayDates'] = $this->data['todayDates'];
        $data['loginDates'] = $this->data['loginDates'];
        
        $this->load->view('user/info', $data);
        $this->load->view('footer');
    }
    
    public function get_users() {
        $params = array();
        $search = NULL;
        $page = 0;
        $num = $this->userModel->count_users();
        
        if ($this->uri->segment(4))
            $page = $this->uri->segment(4);
        
        if ($this->input->post('search') && $this->input->post('search') == 1) {
            $search = new \stdClass;
            
            $search->user_id    = $this->input->post('user_id');
            $search->username   = $this->input->post('username');
            $search->name       = $this->input->post('name');
            $search->created    = $this->input->post('created');
            $search->is_active  = $this->input->post('active');
        }
        
        $params['users'] = $this->userModel->get_users(RPP, $page, $search);
        
        if ($num > RPP) {
            $this->load->library('pagination');
            
            $config['base_url']    = site_url('admin/user/get_users');
            $config['total_rows']  = $num;
            $config['uri_segment'] = 4;
            
            $this->pagination->initialize($config);
            $params['links'] = $this->pagination->create_links();
        }
        
        $this->load->view('admin/users', $params);
        $this->load->view('footer');
    }
    
    public function set_user() {
        $rules = array(
                array(
                    "field"  => "name",
                    "label"  => "Your Name",
                    "rules"  => "required",
                    "errors" => array(
                        "required" => "Please fill in %s."
                    )
                ),
                array(
                    "field"  => "email",
                    "label"  => "E-Mail",
                    "rules"  => "required|valid_email",
                    "errors" => array(
                        "required"    => "Please fill in the %s field.",
                        "valid_email" => "Please enter a valid %s address."
                    )
                ),
                array(
                    "field"  => "username",
                    "label"  => "Username",
                    "rules"  => "trim|required|min_length[5]|max_length[12]|is_unique[users.username]",
                    "errors" => array(
                        "required"   => "Please fill in the %s field.",
                        "min_length" => "The %s must have at least 5 chars.",
                        "max_length" => "The %s must have at most 12 chars.",
                        "is_unique"  => "The chosen %s is already taken."
                    )
                )
            );
        
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $register = array(
                'name'     => $this->input->post('name'),
                'email'    => $this->input->post('email'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('username')
            );
            
            $user = $this->userModel->create($register);
            
            if ($user) {
                $this->session->set_flashdata('info', 'New user created successfully, Sir.');
                redirect ('admin/user/get_users');
            }
            else {
                $this->session->set_flashdata('error', 'Sorry Sir, an error has occurred while creating the new user.');
                redirect ('user/user/create');
            }
        }
        else {
            $this->user();
        }
    }
    
    public function upd_user() {
        $rules = array(
                array(
                    "field"  => "name",
                    "label"  => "Your Name",
                    "rules"  => "required",
                    "errors" => array(
                        "required" => "Please fill in %s."
                    )
                ),
                array(
                    "field"  => "email",
                    "label"  => "E-Mail",
                    "rules"  => "required|valid_email",
                    "errors" => array(
                        "required"    => "Please fill in the %s field.",
                        "valid_email" => "Please enter a valid %s address."
                    )
                ),
                array(
                    "field"  => "username",
                    "label"  => "Username",
                    "rules"  => "trim|required|min_length[5]|max_length[12]",
                    "errors" => array(
                        "required"   => "Please fill in the %s field.",
                        "min_length" => "The %s must have at least 5 chars.",
                        "max_length" => "The %s must have at most 12 chars."
                    )
                ),
                array(
                    "field"  => "reset",
                    "label"  => "Reset Password?",
                    "rules"  => "trim|required|integer|less_than[2]|greater_than_equal_to[0]",
                    "errors" => array(
                        "required"  => "Please fill in the %s field.",
                        "integer"   => "Please choose 'Yes' or 'No'.",
                        "less_than" => "Please choose 'Yes' or 'No'.",
                        "greater_than_equal_to" => "Please choose 'Yes' or 'No'.",
                    )
                )
            );
        
        $user = $this->userModel->user($this->input->post('user_id'));
        
        if ($user->username != $this->input->post('username')) {
            $rules[2]['rules'] .= "|is_unique[users.username]";
            $rules[2]['errors']['is_unique'] = "The chosen %s is already taken.";
        }
        
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $user = array(
                'user_id'   => $user->user_id,
                'name'      => $this->input->post('name'),
                'email'     => $this->input->post('email'),
                'is_active' => $this->input->post('active'),
                'username'  => $this->input->post('username'),
            );
            
            if ($this->input->post('reset') == 1)
                $user['password'] = $user['username'];
            
            $user = $this->userModel->update($user);
            
            if ($user) {
                $this->session->set_flashdata('info', 'User updated successfully, Sir.');
            }
            else {
                $this->session->set_flashdata('error', 'Sorry Sir, an error has occurred while updating the user.');
            }
            
            redirect ('admin/user/get_users');
        }
        else {
            $this->session->set_flashdata('error', $this->form_validation->error_string());
            redirect('admin/user/user/'.$user->user_id);
        }
    }
    
    public function del_user() {
        $user = $this->uri->segment(4);
        
        if ($user != 1) { // Admin user
            $del = $this->userModel->delete($user);

            if (!$del) {
                $this->session->set_flashdata('error', "Sorry Sir, an error has occurred while deleting the user.");
            }
            else {
                $this->session->set_flashdata('info', 'User deleted successfully, Sir.');
            }
        }
        
        redirect('admin/user/get_users');
    }
}
