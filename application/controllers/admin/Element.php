<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once "Admin.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Element
 *
 * @author ricardo
 */
class Element extends Admin {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('elementModel');
    }
    
    public function element() {
        $element = $this->uri->segment(4);
        
        if ($element) 
            $data['element'] = $this->elementModel->element($element);
        
        $data['todayDates'] = $this->data['todayDates'];
        $data['loginDates'] = $this->data['loginDates'];
        
        $this->load->view('admin/formElement', $data);
        $this->load->view('footer');
    }
    
    public function get_elements() {
        $params = array();
        $page = 0;
        $num = $this->elementModel->count_elements();
        
        if ($this->uri->segment(4))
            $page = $this->uri->segment(4);
        
        if ($this->input->post('search') && $this->input->post('search') == 1) {
            $search = new \stdClass;
            
            $search->element    = $this->input->post('user_id');
            $search->created    = $this->input->post('created');
            $search->is_active  = $this->input->post('active');
        }
        
        $params['elements'] = $this->elementModel->get_elements(RPP, $page, $search);
        
        if ($num > RPP) {
            $this->load->library('pagination');
            
            $config['base_url']    = site_url('admin/element/get_elements');
            $config['total_rows']  = $num;
            $config['uri_segment'] = 4;
            
            $this->pagination->initialize($config);
            $params['links'] = $this->pagination->create_links();
        }
        
        $this->load->view('admin/elements', $params);
        $this->load->view('footer');
    }
    
    public function set_element() {
        $rules = array(
            array(
                "field"  => "element",
                "label"  => "New Element",
                "rules"  => "trim|required|min_length[5]|max_length[255]|is_unique[elements.element]",
                "errors" => array(
                    "required"   => "Please fill in the %s field.",
                    "min_length" => "The %s must have at least 5 chars.",
                    "max_length" => "The %s must have at most 255 chars.",
                    "is_unique"  => "The %s already exists."
                )
            )
        );
        
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $element = array(
                'element' => $this->input->post('element')
            );
            
            $element = $this->elementModel->create($element);
            
            if ($element) {
                $this->session->set_flashdata('info', "New element successfully created, Sir.");
                redirect('admin/element/get_elements');
            }
            else {
                $this->session->set_flashdata('error', "Sorry Sir, an error has occurred while creating the new element.");
                redirect('admin/element/element');
            }
        }
        else {
            $this->element();
        }
    }
    
    public function upd_element() {
        $rules = array(
            array(
                "field"  => "element",
                "label"  => "New Element",
                "rules"  => "trim|required|min_length[5]|max_length[255]",
                "errors" => array(
                    "required"   => "Please fill in the %s field.",
                    "min_length" => "The %s must have at least 5 chars.",
                    "max_length" => "The %s must have at most 255 chars."
                )
            )
        );
        
        $element = $this->elementModel->element($this->input->post('element_id'));
        
        if ($element->element != $this->input->post('element')) {
            $rules[0]['rules'] .= "|is_unique[elements.element]";
            $rules[0]['errors']['is_unique'] = "The %s already exists.";
        }
        
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $element = array(
                'element_id' => $element->element_id,
                'element'    => $this->input->post('element'),
                'is_active'  => $this->input->post('active')
            );
            
            $element = $this->elementModel->update($element);
            
            if ($element) {
                $this->session->set_flashdata('info', 'Element updated successfully, Sir.');
                            }
            else {
                $this->session->set_flashdata('error', 'Sorry Sir, an error has occurred while updating the element.');
            }
            
            redirect ('admin/element/get_elements');
        }
        else {
            $this->session->set_flashdata('error', $this->form_validation->error_string());
            redirect('admin/element/element/'.$element->element_id);
        }
    }
    
    public function del_element() { 
        $element = $this->uri->segment(4);
        $del = $this->elementModel->delete($element);
        
        if (!$del) {
            $this->session->set_flashdata('error', "Sorry Sir, an error has occurred while deleting the element.");
        }
        else {
            $this->session->set_flashdata('info', "Element deleted successfully, Sir.");
        }
        
        redirect('admin/element/get_elements');
    }
}
