<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once "Admin.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SavedGames
 *
 * @author ricardo
 */
class Game extends Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('gameModel');
    }
    
    public function game() {
        $game = $this->uri->segment(4);
        
        if ($game) 
            $data['game'] = $this->gameModel->game($game);
        
        if (empty($data['game'])) {
            $this->session->set_flashdata('info', 'Sorry Sir, unable to find that game.');
            redirect('admin/game/get_games');
        }
        
        $data['todayDates'] = $this->data['todayDates'];
        $data['loginDates'] = $this->data['loginDates'];
        
        $this->load->view('admin/formGame', $data);
        $this->load->view('footer');
    }
    
    public function view($game) {
        $game = $this->uri->segment(4);
        
        if ($game) 
            $data['game'] = $this->gameModel->game($game);
        
        if (empty($data['game'])) {
            $this->session->set_flashdata('info', 'Sorry Sir, unable to find that game.');
            redirect('admin/game/get_games');
        }
        
        $data['todayDates'] = $this->data['todayDates'];
        $data['loginDates'] = $this->data['loginDates'];
        
        $this->load->view('game/info', $data);
        $this->load->view('footer');
    }
    
    public function get_games() {
        $params = array();
        $page = 0;
        $num = $this->gameModel->count_games();
        
        if ($this->uri->segment(4))
            $page = $this->uri->segment(4);
        
        if ($this->input->post('search') && $this->input->post('search') == 1) {
            $search = new \stdClass;
            
            $search->config      = $this->input->post('config');
            $search->player_1    = $this->input->post('player1');
            $search->player_2    = $this->input->post('player2');
            $search->lead_winner = $this->input->post('lead_winner');
            $search->started     = $this->input->post('started');
            $search->finished    = $this->input->post('finished');
            $search->is_active   = $this->input->post('active');
        }
        
        $params['games'] = $this->gameModel->get_games(RPP, $page, $search);
        
        if ($num > RPP) {
            $this->load->library('pagination');
            
            $config['base_url']    = site_url('admin/game/get_games');
            $config['total_rows']  = $num;
            $config['uri_segment'] = 4;
            
            $this->pagination->initialize($config);
            $params['links'] = $this->pagination->create_links();
        }
        
        $this->load->view('admin/games', $params);
        $this->load->view('footer');
    }
    
    public function del_game() { 
        $game = $this->uri->segment(4);
        $del = $this->gameModel->delete($game);
        
        if (!$del) {
            $this->session->set_flashdata('error', "Sorry Sir, an error has occurred while deleting the game.");
        }
        else {
            $this->session->set_flashdata('info', "Game deleted successfully, Sir.");
        }
        
        redirect('admin/game/get_games');
    }
}
