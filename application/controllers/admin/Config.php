<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once "Admin.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Configuration
 *
 * @author ricardo
 */
class Config extends Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('configModel');
    }
    
    public function config() {
        $config = $this->uri->segment(4);
        
        if ($config) {
            $data['config'] = $this->configModel->config($config);
            $data['count'] = $this->configModel->count_configs();
        }
            
        $data['todayDates'] = $this->data['todayDates'];
        $data['loginDates'] = $this->data['loginDates'];
        
        $this->load->view('admin/formConfig', $data);
        $this->load->view('footer');
    }
    
    public function get_configs() {
        $params = array();
        
        $params['configs'] = $this->configModel->get_configs();
        $this->load->view('admin/configs', $params);
        $this->load->view('footer');
    }
    
    public function set_config() {
        if ($this->configModel->count_configs() == MAX_CONFIG) {
            $this->session->set_flashdata('warning', "Maximum configuration number reached, Sir.");
            $this->config();
        }
        
        $rules = array(
            array(
                "field"  => "hit",
                "label"  => "Hit Score",
                "rules"  => "trim|required|integer|less_than[128]|greater_than[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "The %s field must be an integer number.",
                    "less_than" => "The %s field must be between 1 and 127.",
                    "greater_than" => "The %s field must be between 1 and 127.",
                ),
            ),
            array(
                "field"  => "miss",
                "label"  => "Miss Score",
                "rules"  => "trim|required|integer|less_than[128]|greater_than[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "The %s field must be an integer number.",
                    "less_than" => "The %s field must be between 1 and 127.",
                    "greater_than" => "The %s field must be between 1 and 127.",
                ),
            ),
            array(
                "field"  => "attempts",
                "label"  => "Attempts",
                "rules"  => "trim|required|integer|less_than[5]|greater_than[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "The %s field must be an integer number.",
                    "less_than" => "The %s field must be between 1 and 4.",
                    "greater_than" => "The %s field must be between 1 and 4.",
                )
            ),
            array(
                "field"  => "timer",
                "label"  => "Timer Value",
                "rules"  => "trim|required|integer|less_than[301]|greater_than[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "The %s field must be an integer number.",
                    "less_than" => "The %s field must be between 1 and 300.",
                    "greater_than" => "The %s field must be between 1 and 300.",
                )
            ),
            array(
                "field"  => "active",
                "label"  => "Make this configuration active?",
                "rules"  => "trim|required|integer|less_than[2]|greater_than_equal_to[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "Please choose 'Yes' or 'No'.",
                    "less_than" => "Please choose 'Yes' or 'No'.",
                    "greater_than_equal_to" => "Please choose 'Yes' or 'No'.",
                )
            )
        );
        
        if ($this->input->post('attempts')) {
            unset($rules[3]);
        }
        
        if ($this->input->post('timer')) {
            unset($rules[2]);
        }
        
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $config = array(
                'score_hit'  => $this->input->post('hit'),
                'score_miss' => $this->input->post('miss'),
                'attempts'   => $this->input->post('attempts'),
                'timer'      => $this->input->post('timer'),
                'is_active'  => $this->input->post('active')
            );
            
            if (!isset($rules[3])) $config['timer'] = 0;
            if (!isset($rules[2])) $config['attempts'] = 0;
            
            $config = $this->configModel->create($config);
            
            if ($config) {
                $this->session->set_flashdata('info', "New configuration successfully created, Sir.");
                redirect('admin/config/get_configs');
            }
            else {
                $this->session->set_flashdata('error', "Sorry Sir, an error has occurred while creating the new configuration.");
                redirect('admin/config/config');
            }
        }
        else {
            $this->config();
        }
    }
    
    public function upd_config() {
        $rules = array(
            array(
                "field"  => "hit",
                "label"  => "Hit Score",
                "rules"  => "trim|required|integer|less_than[128]|greater_than[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "The %s field must be an integer number.",
                    "less_than" => "The %s field must be between 1 and 127.",
                    "greater_than" => "The %s field must be between 1 and 127.",
                ),
            ),
            array(
                "field"  => "miss",
                "label"  => "Miss Score",
                "rules"  => "trim|required|integer|less_than[128]|greater_than[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "The %s field must be an integer number.",
                    "less_than" => "The %s field must be between 1 and 127.",
                    "greater_than" => "The %s field must be between 1 and 127.",
                ),
            ),
            array(
                "field"  => "attempts",
                "label"  => "Attempts",
                "rules"  => "trim|required|integer|less_than[5]|greater_than[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "The %s field must be an integer number.",
                    "less_than" => "The %s field must be between 1 and 4.",
                    "greater_than" => "The %s field must be between 1 and 4.",
                )
            ),
            array(
                "field"  => "timer",
                "label"  => "Timer Value",
                "rules"  => "trim|required|integer|less_than[301]|greater_than[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "The %s field must be an integer number.",
                    "less_than" => "The %s field must be between 1 and 300.",
                    "greater_than" => "The %s field must be between 1 and 300.",
                )
            ),
            array(
                "field"  => "active",
                "label"  => "Make this configuration active?",
                "rules"  => "trim|required|integer|less_than[2]|greater_than_equal_to[0]",
                "errors" => array(
                    "required"  => "Please fill in the %s field.",
                    "integer"   => "Please choose 'Yes' or 'No'.",
                    "less_than" => "Please choose 'Yes' or 'No'.",
                    "greater_than_equal_to" => "Please choose 'Yes' or 'No'.",
                )
            )
        );
        
        if ($this->input->post('attempts')) {
            unset($rules[3]);
        }
        
        if ($this->input->post('timer')) {
            unset($rules[2]);
        }
        
        if ($this->configModel->count_configs() == 1) {
            unset($rules[4]);
        }
        
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run()) {
            $config = array(
                'config_id'  => $this->input->post('config_id'),
                'score_hit'  => $this->input->post('hit'),
                'score_miss' => $this->input->post('miss'),
                'attempts'   => $this->input->post('attempts'),
                'timer'      => $this->input->post('timer'),
                'is_active'  => $this->input->post('active')
            );
            
            if (!isset($rules[3])) $config['timer'] = 0;
            if (!isset($rules[2])) $config['attempts'] = 0;
            if (!isset($rules[4])) $config['is_active'] = 1;
            
            $config = $this->configModel->update($config);
            
            if ($config) {
                $this->session->set_flashdata('info', "Configuration successfully updated, Sir.");
            }
            else {
                $this->session->set_flashdata('error', "Sorry Sir, an error has occurred while updating the new configuration.");
            }
            
            redirect('admin/config/get_configs');
        }
        else {
            $config = $this->configModel->config($this->input->post('config_id'));
            $this->session->set_flashdata('error', $this->form_validation->error_string());
            redirect('admin/config/config/'.$config->config_id);
        }
    }
    
    public function delete_config() {
        $config = $this->uri->segment(4);
        $del = $this->configModel->delete($config);
        
        if (!$del) {
            $this->session->set_flashdata('error', "Sorry Sir, an error has occurred while deleting the configuration.");
        }
        else {
            $this->session->set_flashdata('info', "Configuration deleted successfully, Sir.");
        }
        
        redirect('admin/config/get_configs');
    }
}
