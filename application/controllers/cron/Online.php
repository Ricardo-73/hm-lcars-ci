<?php
if (php_sapi_name() != 'cli') exit();

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Online
 *
 * @author ricardo
 */
class Online {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('onlineModel');
    }
    
    public function index() {
        $online = $this->onlineModel->online();
        $this->session->mark_as_temp($online, CRON_DELTA);
    }
}
