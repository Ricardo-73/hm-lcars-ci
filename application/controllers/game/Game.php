<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Game
 *
 * @author ricardo
 */
class Game extends CI_Controller {
    private $data;
    
    public function __construct() {
        parent::__construct();
        
        if (!$this->session->userdata('user_id'))
            redirect ('user/user/login');
        
        $this->load->model('userModel');
        $this->load->model('gameModel');
        $this->load->library('Calculations');
    }
    
    public function start() {
        
    }
}
