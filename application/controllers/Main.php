<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Main
 *
 * @author ricardo
 */
class Main extends CI_Controller {
    public $sessionId;
    
    public function __construct() {
        parent::__construct();
        
        $this->sessionId = $this->session->userdata();
        $this->load->library('Calculations');
    }
    
    public function index() {
        $method = "";
        
        if (!empty($this->sessionId['user_id'])) {
            $method = "home";
            
            if ($this->sessionId['is_admin'])                
                $method = "admin";
            
            $this->$method();            
        }
        else 
            redirect ('user/user/login');
    }
    
    public function home() { 
        $this->load->model('userModel'); 
        $user = $this->userModel->info($this->sessionId['user_id']);
        
        $name = explode(' ', $this->sessionId['name']);
        $data['title'] = "Welcome ".$name[0]."!";
        
        $data['todayDates'] = Calculations::calc_stardate(date('Y-m-d H:i:s'));
        $data['loginDates'] = Calculations::calc_stardate($this->sessionId['last_login']);
        $data['sessionId']  = $this->sessionId;
        $data['hasPending'] = $user->hasPending;
        
        $this->load->view('header', $data);
        $this->load->view('game/intro', $data);
        $this->load->view('footer');
    }
    
    public function admin() {
        $data['title'] = " Administration";        
        $data['todayDates'] = Calculations::calc_stardate(date('Y-m-d H:i:s'));
        $data['loginDates'] = Calculations::calc_stardate($this->sessionId['last_login']);
        $data['sessionId']  = $this->sessionId;
        
        $this->load->view('header', $data);
        $this->load->view('admin/admin', $data);
        $this->load->view('footer');
    }
}
