<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Calculations
 *
 * @author ricardo
 */
class Calculations {
    
    /** 
     * Calculates stardate for a given date.
     * 
     * earthdate = year + 1 / ndays * (day - 1 + hour/24 + min / 1440 + sec / 86400)
     * 
     * stardate = 1000 * (earthdate - 2323)
     * 
     * @param string $date A date string in Y-m-d H:i:s format.
     * 
     * @return array Array containing stardate and earthdate.
     */
    public function calc_stardate($date = FALSE) {
        $stardate = array();
        $ndays = 365;
        
        if (!$date) 
            $date = date('Y-m-d H:i:s');
        
        if (date('L', strtotime($date)) == 1)
            $ndays += 1;
        
        $stardate['earthdate'] = date('Y', strtotime($date)) + (1 / $ndays) * (date('z', strtotime($date)) + (int)date('H', strtotime($date)) / 24 
                                    + (int)date('i', strtotime($date)) / 1440 + (int)date('s', strtotime('s', $date)) / 86400); 
        $stardate['stardate']  = 1000 * ($stardate['earthdate'] - date('Y', strtotime(STARDATE)));
        $stardate['earthdate'] = $date;
        
        return $stardate;
    }
}